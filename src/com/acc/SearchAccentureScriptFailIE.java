package com.acc;

import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.URL;
import static org.junit.Assert.assertTrue;

public class SearchAccentureScriptFailIE {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
  	  //DesiredCapabilities cap = new DesiredCapabilities();
	  DesiredCapabilities cap = DesiredCapabilities.internetExplorer();
	  System.setProperty("webdriver.ie.driver", "c:/jenkins/tools/selenium/IEDriverServer.exe");
	  //cap.setBrowserName("internet explorer");
	  cap.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
	  //driver= new InternetExplorerDriver(cap);
	  String hubURL = System.getProperty("hubURL");
	  if (hubURL == null) hubURL = "http://172.28.20.37:4445/wd/hub";
          driver= new RemoteWebDriver(new URL(hubURL), cap);
	  baseUrl = "http://www.google.com/";
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  }

  // Taking screenshot on test fail automatically
  @Rule
  public ScreenshotRule screenshotRule = new ScreenshotRule();
  
  @Test
  public void testSearchAccentureScript() throws Exception {
    driver.get(baseUrl + "/?gws_rd=ssl");
    System.out.println("Open google site");
    driver.findElement(By.id("lst-ib")).clear();
    driver.findElement(By.id("lst-ib")).sendKeys("accenture");
    //assertTrue(false);
    System.out.println("Enter accenture in the search field");
  }

/*
  @Test
  public void testThatFails() {
     assertTrue(false);
  }
*/

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
