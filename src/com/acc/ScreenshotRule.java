package com.acc;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;



public class ScreenshotRule extends TestWatcher {

	public static WebDriver driver;
	public boolean bRtn;
	
	public static void setDriver(WebDriver _driver)
	{
		System.out.println("Driver is being set in ScreenshotRule");
		driver = _driver;
	}
	
	
	public String createScreenShotName(Description discription)
	{
		String screenshotName = null;
		try
		{
			if (driver == null) System.out.println("Driver is null going into screenshot");
			Date tempDate = new Date();
			SimpleDateFormat tempFormat = new SimpleDateFormat("yyyy-MM-dd h:mm:ss");
			String tempDateString = tempFormat.format(tempDate).replace(':', '_');
			String[] splited = tempDateString.split("\\s+");
			String tempPart1 = splited[0];
			String tempPart2 = splited[1];
			
			String displayName = discription.getDisplayName();
			screenshotName = tempPart1 + displayName.replace('(', '_').replace(')', '_') + tempPart2;
			
			System.out.println("ScreenshotName: " + screenshotName);
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
			
		}
		return screenshotName + ".png";
	}
	
	
	@Override
	public void failed(Throwable t, Description description)
	{		
		try 
		{
			File tempFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			Thread.sleep(1 * 1000);
			FileUtils.copyFile(tempFile, new File("target/screenshots" + createScreenShotName(description)));
		} catch (Exception e)
		{
			
			e.printStackTrace();
		}
		
	}
	
	
	@Override
	public void finished(Description description)
	{
		String currentMethodName = description.getDisplayName().toString();
		try
		{
			if(!driver.equals(null))
			{
				//driver.close();
				//driver.quit();
			}
		}catch(Exception ex)
		{
			System.out.println("Screenshot error: " + currentMethodName);
		}
		finally
		{
			if(!driver.equals(null))
			{
				//driver.close();				
			}
		}
	}
	
	
	
	
}

