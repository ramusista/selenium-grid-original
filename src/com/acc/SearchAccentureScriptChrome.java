package com.acc;

import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.URL;

public class SearchAccentureScriptChrome {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
  	  //DesiredCapabilities cap = new DesiredCapabilities();
	  DesiredCapabilities cap = DesiredCapabilities.chrome();
	  System.setProperty("webdriver.chrome.driver", "C:\\Selenium Stuff\\chromedriver_win32.exe");
	  //cap.setBrowserName("Internet explorer");
	  //cap.setCapability(ChromeDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
	  //driver= new InternetExplorerDriver(cap);
	  //test//
	  String hubURL = System.getProperty("hubURL");
	  if (hubURL == null) hubURL = "http://192.168.0.140:4444/wd/hub";
          driver= new RemoteWebDriver(new URL(hubURL), cap);
	  baseUrl = "http://www.google.com/";
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  }

  // Taking screenshot on test fail automatically
//  @Rule
//  public ScreenshotRule screenshotRule = new ScreenshotRule();
  
  @Test
  public void testSearchAccentureScript() throws Exception {
    driver.get(baseUrl + "/?gws_rd=ssl");
    System.out.println("Open google site");
    driver.findElement(By.id("lst-ib")).clear();
    driver.findElement(By.id("lst-ib")).sendKeys("accenture");
    System.out.println("Enter accenture in the search field");
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}