package com.acc;

import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.Ignore;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.URL;
import static org.junit.Assert.assertTrue;

public class SearchAccentureScriptFailFX {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    // driver = new FirefoxDriver();
    DesiredCapabilities cap = new DesiredCapabilities();
    cap.setBrowserName("firefox");
    //cap.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
    //driver= new RemoteWebDriver(new URL("http://172.28.20.213:4444/wd/hub"), cap);
    String hubURL = System.getProperty("hubURL");
    if (hubURL == null) hubURL = "http://192.168.0.140:4444/wd/hub";
    System.out.println("Hub URL: " + hubURL);
    driver= new RemoteWebDriver(new URL(hubURL), cap);
    baseUrl = "http://cl-rhd-0005.ba.ssa.gov:8081";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    if (driver == null) System.out.println("Driver is null");
    ScreenshotRule.setDriver(driver);
  }
  
  // Taking screenshot on test fail automatically
  @Rule
  public ScreenshotRule screenshotRule = new ScreenshotRule();
  
  @Test
  public void testSearchAccentureScript() throws Exception {
    driver.get(baseUrl + "/nexus");
    driver.findElement(By.id("quick-search-welcome-field")).clear();
    driver.findElement(By.id("quick-search-welcome-field")).sendKeys("accenture");
    if (driver != null) System.out.println("Driver is alive prior to fail condition");
    assertTrue(false);
    
  }
  
  @After
  public void tearDown() throws Exception {
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
    //System.out.println("Driver is about to be quit");
    //driver.quit();
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
